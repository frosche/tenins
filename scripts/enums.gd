#	Tenins game made in Godot
#	Copyright (C) 2024 Tyoda
#
#	This program is free software; you can redistribute it and/or modify
#	it under the terms of the GNU General Public License version 2 as
#	published by the Free Software Foundation.
class_name Enums


enum Scenes {
	MAIN_MENU,
	LOCAL_MENU,
	OVER_IP_MENU,
	GAME
}


enum GameModes {
	OVER_IP_PVP,
	LOCAL_PVP,
	LOCAL_PVE
}

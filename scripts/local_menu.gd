#	Tenins game made in Godot
#	Copyright (C) 2024 Tyoda
#
#	This program is free software; you can redistribute it and/or modify
#	it under the terms of the GNU General Public License version 2 as
#	published by the Free Software Foundation.
extends Control

@onready var M :Main= get_node("/root/main")


func _ready() -> void:
	$player_1.set_player_name("Player 1")
	$player_2.set_player_name("Player 2")


func _on_back_pressed() -> void:
	M.change_scene(Enums.Scenes.MAIN_MENU)


func _start_game(mode:Enums.GameModes) -> void:
	var game :Game= M.change_scene(Enums.Scenes.GAME)
	game.set_my_player_info($player_1.get_player_name(), $player_1.get_color())
	game.set_other_player_info($player_2.get_player_name(), $player_2.get_color())
	game.set_game_mode(mode)


func _on_player_vs_player_pressed() -> void:
	_start_game(Enums.GameModes.LOCAL_PVP)


func _on_player_vs_ai_pressed() -> void:
	_start_game(Enums.GameModes.LOCAL_PVE)

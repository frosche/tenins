#	Tenins game made in Godot
#	Copyright (C) 2024 Tyoda
#
#	This program is free software; you can redistribute it and/or modify
#	it under the terms of the GNU General Public License version 2 as
#	published by the Free Software Foundation.
extends Control

@onready var M :Main= get_node("/root/main")


func _emit_color_changed(color:Color) -> void:
	emit_signal("color_changed", color)


func _on_exit_button_pressed() -> void:
	get_tree().quit()


func _on_over_ip_button_pressed() -> void:
	M.change_scene(Enums.Scenes.OVER_IP_MENU)


func _on_local_button_pressed() -> void:
	M.change_scene(Enums.Scenes.LOCAL_MENU)

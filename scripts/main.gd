#	Tenins game made in Godot
#	Copyright (C) 2024 Tyoda
#
#	This program is free software; you can redistribute it and/or modify
#	it under the terms of the GNU General Public License version 2 as
#	published by the Free Software Foundation.
extends Node2D
class_name Main

var _scenes := {}

@onready var GUI_ROOT := $gui_root


func _ready() -> void:
	print("  ,     ,")
	print("  (\\,-,/)")
	print("   (o o)")
	print(" ===\\_/===")
	print("   / o")
	print("  / mm\\")
	print("(_\\   /")
	print("   `\"`\"")
	self.log("loading")
	self.load()
	change_scene(Enums.Scenes.MAIN_MENU)


func load() -> void:
	_scenes[Enums.Scenes.MAIN_MENU] = load("res://scenes/main_menu.tscn")
	_scenes[Enums.Scenes.GAME] = load("res://scenes/game.tscn")
	_scenes[Enums.Scenes.LOCAL_MENU] = load("res://scenes/local_menu.tscn")
	_scenes[Enums.Scenes.OVER_IP_MENU] = load("res://scenes/over_ip_menu.tscn")


func change_scene(scene:Enums.Scenes) -> Node:
	var packed :PackedScene= _scenes[scene]
	for child in GUI_ROOT.get_children():
		GUI_ROOT.remove_child(child)
		child.queue_free()
	var new_scene :Node= packed.instantiate()
	GUI_ROOT.add_child(new_scene)
	return new_scene


func _physics_process(_delta) -> void:
	if Input.is_action_just_pressed("quit"):
		get_tree().quit()


func log(message:String) -> void:
	var log_name : String
	# no worky
	if multiplayer.multiplayer_peer == null:
		log_name = "none"
	else:
		log_name = "server" if multiplayer.is_server() else "client"
	print("[" + log_name + "]: " + message)

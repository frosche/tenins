#	Tenins game made in Godot
#	Copyright (C) 2024 Tyoda
#
#	This program is free software; you can redistribute it and/or modify
#	it under the terms of the GNU General Public License version 2 as
#	published by the Free Software Foundation.
extends Control
class_name Game

@onready var M :Main= get_node("/root/main")
@onready var HOST := $host
@onready var CLIENT := $client
@onready var HOST_GOAL := $host_goal
@onready var CLIENT_GOAL := $client_goal
@onready var BALL := $ball

@onready var FEL_BUTTON := $fel_button
@onready var LE_BUTTON := $le_button
@onready var FEL_BUTTON_P_1 := $fel_button_p_1
@onready var LE_BUTTON_P_1 := $le_button_p_1
@onready var FEL_BUTTON_P_2 := $fel_button_p_2
@onready var LE_BUTTON_P_2 := $le_button_p_2

const BALL_SIZE := 10
const BAR_WIDTH := 20
const BAR_HEIGHT := 100

var ball_speed := 500.0
var player_speed := 500.0

var _client_velocity := 0.0
var _ball_velocity : Vector2

var _hide_buttons := !DisplayServer.is_touchscreen_available()

var _score_host := 0
var _score_client := 0

var bumps := 0

@onready var _arena_center := Vector2(495, 295)

var _game_mode := Enums.GameModes.OVER_IP_PVP


func _ready() -> void:
	reset_ball()
	FEL_BUTTON.visible = false
	LE_BUTTON.visible = false
	FEL_BUTTON_P_1.visible = false
	LE_BUTTON_P_1.visible = false
	FEL_BUTTON_P_2.visible = false
	LE_BUTTON_P_2.visible = false


func set_game_mode(game_mode:Enums.GameModes) -> void:
	_game_mode = game_mode
	if !_hide_buttons:
		match _game_mode:
			Enums.GameModes.OVER_IP_PVP, Enums.GameModes.LOCAL_PVE:
				FEL_BUTTON.visible = true
				LE_BUTTON.visible = true
			Enums.GameModes.LOCAL_PVP:
				FEL_BUTTON_P_1.visible = true
				LE_BUTTON_P_1.visible = true
				FEL_BUTTON_P_2.visible = true
				LE_BUTTON_P_2.visible = true


func set_my_player_info(_player_name:String, color:Color) -> void:
	if multiplayer.is_server():
		$host/polygon.color = color
	else:
		$client/polygon.color = color


func set_other_player_info(_player_name:String, color:Color) -> void:
	if multiplayer.is_server():
		$client/polygon.color = color
	else:
		$host/polygon.color = color


func reset_ball() -> void:
	BALL.position = _arena_center
	ball_speed = 500
	bumps = 0
	var ball_angle := randf_range(-PI/3.0, PI/3.0)
	while ball_angle > -PI/12.0 and ball_angle < PI/12.0:
		ball_angle = randf_range(-PI/3.0, PI/3.0)
	_ball_velocity = Vector2(cos(ball_angle), sin(ball_angle))
	if randf() < 0.5:
		_ball_velocity.x *= -1
	_ball_velocity = _ball_velocity.normalized()


func _physics_process(delta) -> void:
	var vel := 0
	match _game_mode:
		Enums.GameModes.LOCAL_PVP:
			_client_velocity = 0
			if Input.is_action_pressed("down") or LE_BUTTON_P_1.is_pressed():
				_client_velocity += 1
			if Input.is_action_pressed("up") or FEL_BUTTON_P_1.is_pressed():
				_client_velocity -= 1
			if Input.is_action_pressed("down2") or LE_BUTTON_P_2.is_pressed():
				vel += 1
			if Input.is_action_pressed("up2") or FEL_BUTTON_P_2.is_pressed():
				vel -= 1
		Enums.GameModes.LOCAL_PVE:
			if Input.is_action_pressed("down") or Input.is_action_pressed("down2") or LE_BUTTON.is_pressed():
				vel += 1
			if Input.is_action_pressed("up") or Input.is_action_pressed("up2") or FEL_BUTTON.is_pressed():
				vel -= 1
			# "ai"
			_client_velocity = 0
			if CLIENT.position.y > BALL.position.y:
				_client_velocity -= 0.75
			elif CLIENT.position.y+BAR_HEIGHT-BALL_SIZE < BALL.position.y:
				_client_velocity += 0.75
		Enums.GameModes.OVER_IP_PVP:
			if Input.is_action_pressed("down") or Input.is_action_pressed("down2") or LE_BUTTON.is_pressed():
				vel += 1
			if Input.is_action_pressed("up") or Input.is_action_pressed("up2") or FEL_BUTTON.is_pressed():
				vel -= 1
			if !multiplayer.is_server() and vel != _client_velocity:
				_client_velocity = vel
				sync_vel_to_server.rpc(vel)
	
	if multiplayer.is_server():
		HOST.move_and_collide(Vector2(0, vel) * player_speed * delta)
		CLIENT.move_and_collide(Vector2(0, _client_velocity) * player_speed * delta)
		var ball_collision :KinematicCollision2D= BALL.move_and_collide(_ball_velocity * ball_speed * delta)
		if ball_collision:
			var collider := ball_collision.get_collider()
			match collider:
				HOST, CLIENT:
					var bar_top : float
					var bar_bottom : float
					_ball_velocity.x  *= -1
					bumps += 1
					if bumps % 5 == 0:
						ball_speed *= 1.1
					match collider:
						HOST:
							bar_top = HOST.position.y
							bar_bottom = HOST.position.y+BAR_HEIGHT
							BALL.position.x = 50
						CLIENT:
							bar_top = CLIENT.position.y
							bar_bottom = CLIENT.position.y+BAR_HEIGHT
							BALL.position.x = 950 - BALL_SIZE
					if (BALL.position.y < bar_top and BALL.position.y+BALL_SIZE-0.5 <= bar_top) \
						or (BALL.position.y+BALL_SIZE > bar_bottom and BALL.position.y+0.5 >= bar_bottom):
						_ball_velocity.y  *= -1
					
				HOST_GOAL:
					reset_ball()
					_score_client += 1
					sync_score.rpc(_score_host, _score_client)
				CLIENT_GOAL:
					reset_ball()
					_score_host += 1
					sync_score.rpc(_score_host, _score_client)
				_:
					_ball_velocity.y  *= -1
		sync_pos_to_client.rpc(HOST.position, CLIENT.position, BALL.position)


@rpc("any_peer", "call_remote", "reliable")
func sync_vel_to_server(vertical_vel:int) -> void:
	if vertical_vel > 0:
		_client_velocity = 1
	elif vertical_vel < 0:
		_client_velocity = -1
	else:
		_client_velocity = 0


@rpc("authority", "call_remote", "unreliable")
func sync_pos_to_client(host_pos:Vector2, client_pos:Vector2, ball_position:Vector2) -> void:
	HOST.position = host_pos
	CLIENT.position = client_pos
	BALL.position = ball_position


@rpc("authority", "call_local", "reliable")
func sync_score(host_score:int, client_score:int) -> void:
	$score_host.text = str(host_score)
	$score_client.text = str(client_score)

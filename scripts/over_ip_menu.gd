#	Tenins game made in Godot
#	Copyright (C) 2024 Tyoda
#
#	This program is free software; you can redistribute it and/or modify
#	it under the terms of the GNU General Public License version 2 as
#	published by the Free Software Foundation.
extends Control

@onready var M :Main= get_node("/root/main")

const MAX_CLIENTS := 1

var _other_player_info = null

var _sent_info := false
var _received_info := false


func _ready() -> void:
	multiplayer.peer_connected.connect(_on_player_connected)


func _process(_delta) -> void:
	if _sent_info and _received_info:
		var game := M.change_scene(Enums.Scenes.GAME)
		game.set_my_player_info($player_customizer.get_player_name(), $player_customizer.get_color())
		game.set_other_player_info(_other_player_info["name"], _other_player_info["color"])
		game.set_game_mode(Enums.GameModes.OVER_IP_PVP)


func _on_host_pressed() -> void:
	M.log("starting server")
	var server := ENetMultiplayerPeer.new()
	server.create_server(int($port.value), MAX_CLIENTS)
	multiplayer.multiplayer_peer = server
	M.log("started server")


func _on_join_pressed() -> void:
	M.log("creating client")
	var client := ENetMultiplayerPeer.new()
	client.create_client($ip.text, int($port.value))
	multiplayer.multiplayer_peer = client
	M.log("client created")


func _on_back_pressed() -> void:
	M.change_scene(Enums.Scenes.MAIN_MENU)


@rpc("any_peer", "call_remote", "reliable")
func sync_player_info(player_name:String, color:Color) -> void:
	# bad
	_other_player_info = {
		"name" : player_name,
		"color" : color
	}
	_received_info = true


func _on_player_connected(id:int) -> void:
	sync_player_info.rpc_id(id, $player_customizer.get_player_name(), $player_customizer.get_color())
	_sent_info = true


func _emit_ip_changed(ip:String) -> void:
	emit_signal("ip_changed", ip)


func _emit_port_changed(port:int) -> void:
	emit_signal("port_changed", port)

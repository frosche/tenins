#	Tenins game made in Godot
#	Copyright (C) 2024 Tyoda
#
#	This program is free software; you can redistribute it and/or modify
#	it under the terms of the GNU General Public License version 2 as
#	published by the Free Software Foundation.
extends Control

signal color_changed(color:Color)
signal name_changed(new_name:String)


func set_player_name(new_name:String) -> void:
	$name.text = new_name


func set_color(color:Color) -> void:
	$r.color = color.r
	$g.color = color.g
	$b.color = color.b


func _ready() -> void:
	$r.value = int(randf() * 256)
	$g.value = int(randf() * 256)
	$b.value = int(randf() * 256)


func get_player_name() -> String:
	return $name.text


func get_color() -> Color:
	return Color($r.value/255.0, $g.value/255.0, $b.value/255.0)


func _on_color_changed(_value) -> void:
	var new_color := get_color()
	_emit_color_changed(new_color)
	$color.color = new_color


func _on_name_changed() -> void:
	_emit_name_changed($name.text)


func _emit_color_changed(color:Color) -> void:
	emit_signal("color_changed", color)


func _emit_name_changed(new_name:String) -> void:
	emit_signal("name_changed", new_name)
